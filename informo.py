#!/usr/bin/python3

import argparse
import re
import sqlite3

book_paths = {
    'u1': {'xpr': 'ukulele',
           'gitlab': 'https://gitlab.com/rwmpelstilzchen/ukulele/blob/master/\
muzika%C4%B5oj/'},
    'ok': {'xpr': 'ocarina',
           'gitlab': 'https://gitlab.com/rwmpelstilzchen/muzikvadratoj/blob/\
master/muzikajxoj/'},
    'mk': {'xpr': 'muzikoloroj',
           'gitlab': 'https://gitlab.com/rwmpelstilzchen/muzikvadratoj/blob/\
master/muzikajxoj/'},
}

book_in_db = {
    'u1': 'u1',
    'ok': 'mk',
    'mk': 'mk',
}

output_formats = {
    'u1': ('pdf', 'png', 'midi', 'mp3', 'lilypond'),
    'ok': ('midi', 'mp3', 'tex'),
    'mk': ('midi', 'mp3', 'tex'),
}

DBFILE = '../datumaro/datumaro.db'
DEFAULT_LANG = 'he'
DEFAULT_BOOK = 'u1'


dictionary = {
    ('he', 'index'): 'תוכן עניינים',
    ('eo', 'index'): 'Indekso',
    ('he', 'pieces'): 'מנגינות',
    ('eo', 'pieces'): 'muzikaĵoj',
    ('he', 'lyricist'): 'מילים',
    ('eo', 'lyricist'): 'poeto',
    ('he', 'www'): 'קישורים',
    ('eo', 'www'): 'TTT-a ligiloj',
    ('he', 'score'): 'תווים',
    ('eo', 'score'): 'Partituro',
    ('he', 'talmid'): 'תווים ב„ספר השירים לתלמיד”',
    ('eo', 'talmid'): 'Partituro en «ספר השירים לתלמיד»',
    ('he', 'recording'): 'ביצוע מוקלט',
    ('eo', 'recording'): 'Sonregistro',
    ('he', 'zemereshet'): 'דף השיר באתר „זמרשת”',
    ('eo', 'zemereshet'): 'La retpaĝo de la kanto en «Zemereŝet»',
    ('he', 'withscore'): '(כולל תווים)',
    ('eo', 'withscore'): '(kun partituro)',
}


def latex(text):
    text = re.sub(r'\{\\nekonata\}', r'*nekonata*', text)
    text = re.sub(r'\{\\popola\}', r'*popola*', text)
    text = re.sub(r'\{\\sennoma\}', r'*sennoma*', text)
    p = re.compile(r'\~')
    text = p.sub(' ', text)
    m = re.match('.*\\{(.*)}.*', text)
    if m:
        return m.group(1)
    else:
        return text


def talmid(data, language):
    m = re.match(r'(.*):(.*)', data)
    if language == 'he':
        s = "חלק "
        if m.group(1) == '1':
            s += "א׳"
        else:
            s += "ב׳"
        s += ", עמ׳ "
        s += m.group(2)
        return s
    elif language == 'eo':
        s = ""
        if m.group(1) == '1':
            s += "unua"
        else:
            s += "dua"
        s += " kajero, "
        s += m.group(2)
        s += "-a paĝo"
        return s


def level(row, book):
    if book == 'u1':
        if row['u1-rango'] != '1':
            if row['u1-rango'] == '1/2':
                return(" <small>🐞</small>")
            elif row['u1-rango'] == '2':
                return(" 🐞")
            elif row['u1-rango'] == '3':
                return(" 🐞🐞")
    elif book == 'ok':
        if row['mk-oc'] == 6:
            return(" ⠿")
    return ""


def create_button(label, url):
    print("<a class=\"pure-button\" " +
          "style=\"font-size: 80%; padding: 0.5ex\" href=\"" + url + "\">" +
          label +
          "</a>")


def print_metatag(dbname, dicname, language, row):
        if row[dbname + language] is not None:
            print("*" + dictionary[language, dicname] + "*:&nbsp;" +
                  "" + row[dbname + language] + "  ")


def output_index(database, book, language):
    print("\n### " + dictionary[language, 'index'] + "\n")
    c = database.cursor()
    for row in c.execute("SELECT kategorio, COUNT(*) as cnt\
                         FROM muzikaĵoj\
                         WHERE " + book_in_db[book] + " = 'vv'\
                         GROUP BY kategorio\
                         ORDER BY COUNT(*) DESC"):
        cn = database.cursor()
        cn.execute("SELECT * FROM kategorioj WHERE kategorio='" +
                   row['kategorio'] + "'")
        categoryInfo = cn.fetchone()
        if categoryInfo is not None:
            print("* [" + categoryInfo['nomo-' + language] + "]" +
                  "(#kat-" + categoryInfo['kategorio'] + "-" + language + ")" +
                  " <small>" + "(" + str(row['cnt']) + " " +
                  dictionary[language, 'pieces'] + ")" + "</small>")


def process_category(database, book, language, category):
    c = database.cursor()
    c.execute("SELECT * FROM kategorioj WHERE kategorio='" + category + "'")
    categoryInfo = c.fetchone()
    if categoryInfo is not None:
        print("\n### <a name=" +
              "\"kat-" + categoryInfo['kategorio'] + "-" + language + "\">" +
              "</a> " +
              categoryInfo["nomo-" + language] + "\n")

    for row in c.execute("SELECT * FROM muzikaĵoj WHERE " +
                         book_in_db[book] + " ='vv' AND kategorio='" +
                         category + "'\
                         ORDER BY `titolo-he`"):
        print("\n<div style=\"margin-bottom: 3em\">")
        print("<div markdown=\"1\">")
        if language == 'he':
            print("<div markdown=\"span\" name=\"SpanModeOverride\" style=\"float: right;\
                  border-top: 1px solid gray;\
                  border-bottom: 1px solid gray\">")
        else:
            print("<div markdown=\"span\" name=\"SpanModeOverride\" style=\"float: left;\
                  border-top: 1px solid gray;\
                  border-bottom: 1px solid gray\">")
        if row['ikono-ttt'] is not None:
            print(row['ikono-ttt'], end='')
        else:
            print(row['ikono'], end='')
        print(" **" + latex(row['titolo-' + language]) + "**", end='')
        if row['titolo-xx'] is not None:
            print(" (" + latex(row['titolo-xx']) + ")", end='')
        print(" / ", end='')
        if row['komponisto-' + language + '-ttt'] is not None:
            print("[" + latex(row['komponisto-' + language]) + "]" +
                  "(" + row['komponisto-' + language + '-ttt'] + ")", end='')
        else:
            print(latex(row['komponisto-' + language]), end='')
        if row['komponisto-xx'] is not None:
            print(" (" + latex(row['komponisto-xx']) + ")", end='')
        if row['poeto-' + language] is not None:
            print("; " + dictionary[language, 'lyricist'] + ": ", end='')
            if row['poeto-' + language + '-ttt'] is not None:
                print("[" + latex(row['poeto-' + language]) + "]" +
                      "(" + row['poeto-' + language + '-ttt'] + ")", end='')
            else:
                print(latex(row['poeto-' + language]), end='')
        if row['poeto-xx'] is not None:
            print(" (" + latex(row['poeto-xx']) + ")", end='')
        print(level(row, book))
        print("</div>")
        if language == 'he':
            print("<div markdown=\"span\" name=\"SpanModeOverride\" style=\"float: left; direction: ltr\">")
        else:
            print("<div markdown=\"span\" name=\"SpanModeOverride\" style=\"float: right; direction: ltr\">")
        if 'pdf' in output_formats[book]:
            create_button("PDF", PDF_PATH + row['dosiernomo'] + ".pdf")
        if 'png' in output_formats[book]:
            create_button("PNG", PNG_PATH + row['dosiernomo'] + ".png")
        if 'midi' in output_formats[book]:
            create_button("MIDI", MIDI_PATH + row['dosiernomo'] + ".midi")
        if 'mp3' in output_formats[book]:
            create_button("MP3", MP3_PATH + row['dosiernomo'] + ".mp3")
        if 'lilypond' in output_formats[book]:
            create_button("Ly",
                        SOURCE_PATH +
                        row['dosiernomo'] + "/" +
                        row['dosiernomo'] + ".ly")
        if 'tex' in output_formats[book]:
            create_button("TeX", SOURCE_PATH + row['dosiernomo'] + ".tex")
        print("</div>")
        print("</div>")
        print("<br style=\"clear: both\" />")

        print("<div markdown=\"1\">")
        if row['noto-' + language] is not None:
            print()
            print(row['noto-' + language])
            print()
        print_metatag('ttt-', 'www', language, row)
        if row['partituro-' + language] != 'zemereshet':
            print_metatag('partituro-', 'score', language, row)
        if row['partituro-talmid'] is not None:
            print("*" + dictionary[language, 'talmid'] + "*:&nbsp;", end='')
            print(talmid(row['partituro-talmid'], language) + ".  ")
        print_metatag('sono-', 'recording', language, row)
        if row['zemereshet'] is not None:
            print("[" + dictionary[language, 'zemereshet'] + "]" +
                  "(http://www.zemereshet.co.il/song.asp?id=" +
                  row['zemereshet'] + ")", end="")
            if row['partituro-' + language] == 'zemereshet':
                print(" " + dictionary[language, 'withscore'] + ".")
            else:
                print(".")
        print("</div>")
        print("</div>")


def process_data(database, book, language):
    global PDF_PATH
    global PNG_PATH
    global MIDI_PATH
    global MP3_PATH
    global SOURCE_PATH
    PDF_PATH = '{filename}/senmova/pdf/' +\
        book_paths[book]['xpr'] + '/'
    PNG_PATH = '{filename}/senmova/png/' +\
        book_paths[book]['xpr'] + '/'
    MIDI_PATH = '{filename}/senmova/midi/' +\
        book_paths[book]['xpr'] + '/'
    MP3_PATH = '{filename}/senmova/mp3/' +\
        book_paths[book]['xpr'] + '/'
    SOURCE_PATH = book_paths[book]['gitlab']


    c = database.cursor()
    isFirstCategory = True
    output_index(database, book, language)
    for row in c.execute("SELECT kategorio, COUNT(*)\
                         FROM muzikaĵoj\
                         WHERE " + book_in_db[book] + " = 'vv'\
                         GROUP BY kategorio\
                         ORDER BY COUNT(*) DESC"):
        if not isFirstCategory:
            print("")
        else:
            isFirstCategory = False
        if row[1] > 0:
            process_category(database, book, language, row[0])


def main():
    #print("title: ⚙\n")  # TODO: removeme
    #print("<div dir=\"ltr\" markdown=\"1\">")
    #print("<style>.mk {display: none}</style>\n")
    parser = argparse.ArgumentParser(
        description="⚙")
    parser.add_argument("-d", "--database", type=str,
                        default=DBFILE,
                        help="database file")
    parser.add_argument("-l", "--lang", type=str, default=DEFAULT_LANG,
                        help="language")
    parser.add_argument("-b", "--book", type=str, default=DEFAULT_BOOK,
                        help="book")
    args = parser.parse_args()

    dbconn = sqlite3.connect(args.database)
    dbconn.row_factory = sqlite3.Row
    process_data(dbconn, args.book, args.lang)
    #print("</div>")


if __name__ == "__main__":
    main()
